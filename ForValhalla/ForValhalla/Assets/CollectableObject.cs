using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class CollectableObject : InteractableObject
{
    private OwnershipTransfer _ownership;
    public CollectableItem _collectable;
    protected override void Awake()
    {
        PhotonNetwork.AddCallbackTarget(this);
        base.Awake();

        _ownership = GetComponent<OwnershipTransfer>();
        _positionSetter.SetInteractableObject(InteractionController.Instance.SingleInteraction);
    }
    protected override void Start()
    {
        interactionBar = InteractionController.Instance.SingleInteraction;
        _positionSetter.ui_Element = interactionBar;
        base.Start();
    }
    protected override void Update()
    {
        base.Update();
    }
    void OnEnable()
    {
        InteractionClick.OnSingleInteractionComplete += CollectObjectOnClick;
    }
    void OnDisable()
    {
        InteractionClick.OnSingleInteractionComplete -= CollectObjectOnClick;
    }
    private void OnDestroy()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    void CollectObjectOnClick()
    {
        Vector3 offset = _localPlayer.transform.position - transform.position;
        float sqrLen = offset.sqrMagnitude;

        // square the distance we compare with
        if (sqrLen < closeDistance * closeDistance)
        {
            _ownership.RequestOwnership();
            _localPlayer.SetCarryingItem(_collectable);
            DestroyObject(photonView.ViewID);
        }
    }


    public void DestroyObject(int view)
    {
        base.photonView.RPC("RPC_DestroyCollectable", RpcTarget.All, view);  // Need to have RpcTarget set to All!
    }

    [PunRPC]
    public void RPC_DestroyCollectable(int viewId)
    {
        var Object = PhotonView.Find(viewId);
        Destroy(Object.gameObject);
    }

    public void RequestOwnership()
    {
        base.photonView.RequestOwnership();
    }

    public void OnOwnershipRequest(PhotonView targetView, Player requestingPlayer)
    {
        if (targetView != base.photonView) return;
        //Add checks here if you want to add some condition/rule to transfer 
        base.photonView.TransferOwnership(requestingPlayer);
    }

    public void OnOwnershipTransfered(PhotonView targetView, Player previousOwner)
    {
        if (targetView != base.photonView) return;
    }

    public void OnOwnershipTransferFailed(PhotonView targetView, Player senderOfFailedRequest)
    {
        Debug.Log("Transfer Ownership Failed!!!!");
        // throw new System.NotImplementedException();
    }

}

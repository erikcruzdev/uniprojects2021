using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public int WaveSize;

    private int _spawnedEnemies = 0;

    public GameObject EnemyPrefab;

    public float TimeToSpawn;

    private float _timer;

    public List<Transform> SpawnPosition = new List<Transform>();

    public delegate void OnCounterFinish();
    public static event OnCounterFinish OnCounterEnd;
   
    // Start is called before the first frame update
    void OnEnable()
    {
        OnCounterEnd += SpawnEnemy;
    }

    void OnDisable()
    {
        OnCounterEnd -= SpawnEnemy;
    }

    void SpawnEnemy()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            var index = Random.Range(0, SpawnPosition.Count);
            MasterManager.NetworkInstantiate(EnemyPrefab, SpawnPosition[index].position, Quaternion.identity);
            _spawnedEnemies++;
        }
    }
    // Update is called once per frame
    void Update()
    {
        _timer += Time.deltaTime;
        if (_timer >= TimeToSpawn && _spawnedEnemies < WaveSize) {
            _timer = 0;
            if (OnCounterEnd != null)
                OnCounterEnd();
        }
    }
}

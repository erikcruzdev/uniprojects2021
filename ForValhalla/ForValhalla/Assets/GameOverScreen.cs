using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class GameOverScreen : MonoBehaviour
{
    public static GameOverScreen Instance;
    [SerializeField] CanvasGroup _group;
    [SerializeField] TextMeshProUGUI _text;

    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
        _group = GetComponent<CanvasGroup>();
        _group.alpha = 0;
    }

    public static void Bla() {
        Debug.Log("blabalbal");
    
    }

    public void SetGameOverTextAndEnable(string text, float duration) {
        _text.text = text;

        _group.DOKill();
        _group.DOFade(1f, duration);
    
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class InteractableObject : MonoBehaviourPun, IPunObservable
{

    // detects when the other transform is closer than closeDistance
    // this is faster than using Vector3.magnitude
    [SerializeField]
    private bool _canInteractWithBothClasses;
    [SerializeField]
    private CharacterClass _classToInteract;
    protected PlayableCharacterBase _localPlayer;
    public float closeDistance = 5.0f;
    public GameObject Outline;
    [HideInInspector]
    public InteractableUIElement interactionBar;
    protected InteractionOnTopOfObject _positionSetter;

    protected bool IsClose = false;
    protected bool HasRegisteredEvent = false;

    protected bool _isComplete = false;
    // Start is called before the first frame update
    protected virtual void Awake()
    {
        _positionSetter = GetComponent<InteractionOnTopOfObject>();
    }
    protected virtual void Start()
    {
        Outline.SetActive(false);

        foreach (PlayableCharacterBase character in SceneManager.Instance.PlayersOnRoom)
        {
            if (character.IsLocalPlayer)
            {
                _localPlayer = character;
               // Debug.Log("GOT MY LOCAL PLAYER!!");
            }
        }
    }

    public void OnFinishInteraction() {
    
    }


    // Update is called once per frame
    protected virtual void Update()
    {
        if (_localPlayer == null)
        {
            foreach (PlayableCharacterBase character in SceneManager.Instance.PlayersOnRoom)
            {
                if (character.IsLocalPlayer)
                {
                    _localPlayer = character;
                    //Debug.Log("GOT MY LOCAL PLAYER!!");
                }
            }
        }
        if (_localPlayer != null && (_canInteractWithBothClasses || _localPlayer.CharacterClass == _classToInteract))
        {
            Vector3 offset = _localPlayer.transform.position - transform.position;
            float sqrLen = offset.sqrMagnitude;

            // square the distance we compare with
            if (sqrLen < closeDistance * closeDistance)
            {
                IsClose = true;
                // Debug.Log("The other transform is close to me!");
                Outline.SetActive(true);
                if(_positionSetter!=null) _positionSetter.SetPosition(gameObject);
            }
            else
            {
                IsClose = false;
                if (_positionSetter != null && _positionSetter.ui_Element.CurrentTarget == gameObject) _positionSetter.Hide();
                Outline.SetActive(false);
                //Debug.Log("The other transform is NOT close to me!");
            }
        }
        else
        {
            //Debug.Log("Local player is null");
        }

    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //throw new System.NotImplementedException();
    }
}

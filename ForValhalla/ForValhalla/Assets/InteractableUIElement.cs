using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class InteractableUIElement : MonoBehaviour
{

    public const string FILL_FLOAT = "Fill";
    public Image _fillImage;
    public TextMeshProUGUI _text;
    protected float _fill;
    public float Fill => _fill;
    public float DecaySpeed;
    public float PressFill = 0.20f;
    public Animator _animator;
    public KeyCode Key;
    public string KeyBinding;
    protected CanvasGroup _group;
    protected GameObject _currentTarget;
    public GameObject CurrentTarget => _currentTarget;

    // Start is called before the first frame update
    protected virtual void Awake()
    {
        _animator = GetComponent<Animator>();
        _group = GetComponentInParent<CanvasGroup>();
    }
    protected virtual void Start()
    {
        _group.alpha = 0;
        _text.text = KeyBinding;
    }

    public void SetPosition(GameObject target)
    {
        if (target != null)
        {
            _currentTarget = target;
            //Debug.Log("Show", gameObject);
            //InteractionButton.SetActive(true);
            _group.DOFade(1.0f, 0.5f);
        }
    }

    public void Hide()
    {

        _currentTarget = null;
        //Debug.Log("Hideeeee", gameObject);
        _group.DOFade(0.0f, 0.5f);


        //InteractionButton.SetActive(false);
    }
    public void RestartBar()
    {
        _fill = 0;
        Hide();
    }
    // Update is called once per frame
    protected virtual void Update()
    {
        //if(PhotonNetwork.)
        if (Input.GetKeyDown(Key))
        {
            _fill = _fill + PressFill;
        }
        _fillImage.fillAmount = _fill;
        _animator.SetFloat(FILL_FLOAT, _fill);
        if (CurrentTarget == null) Hide();
    }
}

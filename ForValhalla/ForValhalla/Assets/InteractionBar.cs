using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class InteractionBar : InteractableUIElement
{
    public delegate void OnCompleteMultipleClick();
    public event OnCompleteMultipleClick OnMultipleInteractionComplete;

 
    void OnEnable()
    {
       
        OnMultipleInteractionComplete += RestartBar;
    }

  

    void OnDisable()
    {
        OnMultipleInteractionComplete -= RestartBar;
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        if (_fill > 0 && _fill < 1.0f)
        {
            _fill = _fill - (Time.deltaTime * DecaySpeed);
        }

        //if(PhotonNetwork.)
        //if (Input.GetKeyDown(Key)) {
        //    _fill = _fill + PressFill;
        //}
        //_fillImage.fillAmount = _fill;
        //_animator.SetFloat(FILL_FLOAT, _fill);

        if (Mathf.Approximately(_fill, 1.0f) || _fill > 1.0f)
        {
            Debug.Log("COMPLETED!!!!");
            if (OnMultipleInteractionComplete != null)
                OnMultipleInteractionComplete();
        }
    }
}

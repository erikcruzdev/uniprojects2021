using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class InteractionClick : InteractableUIElement
{
    public delegate void OnCompleteSingleClick();
    public static event OnCompleteSingleClick OnSingleInteractionComplete;

   
    void OnEnable()
    {
        OnSingleInteractionComplete += RestartBar;
    }


    void OnDisable()
    {
        OnSingleInteractionComplete -= RestartBar;
    }
  
    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        if (Mathf.Approximately(_fill, 1.0f) || _fill > 1.0f)
        {
            if (OnSingleInteractionComplete != null)
                OnSingleInteractionComplete();
        }
    }
}

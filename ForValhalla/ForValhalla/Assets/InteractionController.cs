using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionController : MonoBehaviour
{
    public static InteractionController Instance;
    public InteractableUIElement FillInteraction;
    public InteractableUIElement SingleInteraction;
    public InteractableUIElement TileInteraction;
    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

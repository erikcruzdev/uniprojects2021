using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;


public class InteractionOnTopOfObject : MonoBehaviour
{
    [ReadOnly]
    public InteractableUIElement ui_Element;
    public float yOffset = 100f;
    // Start is called before the first frame update

    protected void Awake()
    {
      
    }
    public void SetInteractableObject(InteractableUIElement target) {
        ui_Element = target;
    }

    // Update is called once per frame
    public void SetPosition(GameObject target)
    {
        ui_Element.SetPosition(target);
        Vector3 position = Camera.main.WorldToScreenPoint(this.transform.position);
        position.y = position.y + yOffset;
        ui_Element.transform.position = position;
    }

    public void Hide() {
        ui_Element.Hide();
    }
}

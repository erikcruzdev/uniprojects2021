using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class LifeBar : MonoBehaviourPunCallbacks
{
    public static LifeBar Instance;
    [SerializeField]
    private Slider _slider;
    // Start is called before the first frame update
    private void Awake()
    {
        Instance = this;
    }
    public void UpdateLifeBar(float amount, bool isDamage) {
       
        base.photonView.RPC("RPC_SyncLifeBar", RpcTarget.All, amount, isDamage);  // Need to have RpcTarget set to All!       
    }

    [PunRPC]
    public void RPC_SyncLifeBar(float amount, bool isDamage) {
        if (isDamage)
            _slider.value -= amount;
        else
            _slider.value += amount;
        CheckIfIsOver();
    }

    public void CheckIfIsOver() {
        if (_slider.value <= 0) {
            GameOverScreen.Instance.SetGameOverTextAndEnable("Game Over", 1f);
            //Debug.Log("gameOver")
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M)) {
            UpdateLifeBar(0.1f, false);
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            UpdateLifeBar(0.1f, true);
        }
    }
}

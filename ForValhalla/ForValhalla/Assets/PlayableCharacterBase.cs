using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayableCharacterBase : MonoBehaviourPun, IPunObservable
{
    public CharacterClass CharacterClass;

    private SpriteRenderer _renderer;

    private bool _isCarryingItem;

    private bool _canDropItem;
    public bool IsCarryingItem => _isCarryingItem;
    
    public bool IsLocalPlayer { get; private set; }

    public KeyCode DropItemKey;

    public CollectableItem HoldingItem;

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //throw new System.NotImplementedException();
    }

    public void SetCarryingItem(CollectableItem holdingItem) {
        HoldingItem = holdingItem;
        _isCarryingItem = true;
    }
    // Start is called before the first frame update
    private void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
       
    }
    private void Start()
    {
        IsLocalPlayer = base.photonView.IsMine;
        SceneManager.Instance.AddPlayerToList(this);
        if ((CharacterClass)base.photonView.Owner.CustomProperties["CharacterClass"] == CharacterClass.DWARF)
        {
            _renderer.color = Color.blue;
            CharacterClass = CharacterClass.DWARF;
        }
        else
        {
            _renderer.color = Color.green;
            CharacterClass = CharacterClass.WARRIOR;
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(DropItemKey) && _isCarryingItem && _canDropItem) {
            Debug.Log("DROP ITEM");
            Vector2 position = new Vector2(transform.position.x + 0.3f, transform.position.y);
            MasterManager.NetworkInstantiate(HoldingItem.PrefabToSpawn, position, Quaternion.identity);
            HoldingItem = null;
            _canDropItem = false;
            _isCarryingItem = false;
        }
        if (_isCarryingItem) _canDropItem = true;

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    public static SceneManager Instance;

    private List<PlayableCharacterBase> _playableCharacterBases = new List<PlayableCharacterBase>();
    public List<PlayableCharacterBase> PlayersOnRoom => _playableCharacterBases;
    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
    }

    public void AddPlayerToList(PlayableCharacterBase character) {
        _playableCharacterBases.Add(character);
    }
    // Update is called once per frame
    void Update()
    {
        
    }

}

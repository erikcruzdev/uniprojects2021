using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum CharacterClass {
    WARRIOR,
    DWARF,
}
public class CharacterClassCustomProperty : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _currentClass;
    [SerializeField] private TextMeshProUGUI _text;
    private ExitGames.Client.Photon.Hashtable _myCustomProperty = new ExitGames.Client.Photon.Hashtable();
    public CharacterClass _currentValue = CharacterClass.WARRIOR;

    private void SetPlayerCharacterClass() {
        _currentValue = _currentValue == CharacterClass.WARRIOR ? CharacterClass.DWARF : CharacterClass.WARRIOR;
        // System.Random rnd = new System.Random();
        //CharacterClass result = rnd.Next(0, 99);
        SetInfoOnStart();
    }

    void SetInfoOnStart() {
        _currentClass.text = _currentValue == CharacterClass.WARRIOR ? "Current Class: Warrior" : "Current Class: Dwarf";
        var text = _currentValue == CharacterClass.WARRIOR ? " Change to Dwarf" : "Change to Warrior";
        _text.text = text.ToString();

        _myCustomProperty["CharacterClass"] = _currentValue;
        // to remove
        //_myCustomProperty.Remove("RandomNumber");
        // PhotonNetwork.LocalPlayer.CustomProperties = _myCustomProperty;
        PhotonNetwork.SetPlayerCustomProperties(_myCustomProperty);

        if (PhotonNetwork.InRoom)
        {
            ExitGames.Client.Photon.Hashtable propHash = new ExitGames.Client.Photon.Hashtable();
            propHash.Add("CharacterClass", PhotonNetwork.LocalPlayer.CustomProperties["CharacterClass"]);
            PhotonNetwork.CurrentRoom.SetCustomProperties(propHash);
        }
    }

    private void Start()
    {
        SetInfoOnStart();
    }
    public void OnClick_Button() {
        SetPlayerCharacterClass();
    }
}

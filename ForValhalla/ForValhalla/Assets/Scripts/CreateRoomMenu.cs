using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Photon.Realtime;

public class CreateRoomMenu : MonoBehaviourPunCallbacks
{
    [SerializeField] private TextMeshProUGUI _roomName;

    private RoomsCanvases _roomsCanvases;

    public void FirstInitialize(RoomsCanvases canvas)
    {
        _roomsCanvases = canvas;
    }
    public void OnClick_CreateRoom() {
        //CreateRoom
        //JoinOrCreate
        if (!PhotonNetwork.IsConnected) return;
        RoomOptions options = new RoomOptions();
        options.PublishUserId = true;
        options.BroadcastPropsChangeToAll = true;
        options.MaxPlayers = 2;
        //string room_name_text = string.Concat(_roomName.text);
        PhotonNetwork.JoinOrCreateRoom(_roomName.text, options, TypedLobby.Default);

    }

    public override void OnCreatedRoom()
    {
        Debug.Log("Create room successfully", this);
         ExitGames.Client.Photon.Hashtable _myCustomProperty = new ExitGames.Client.Photon.Hashtable();
        PhotonNetwork.CurrentRoom.CustomProperties["CharacterClass"] = PhotonNetwork.LocalPlayer.CustomProperties["CharacterClass"];

        ExitGames.Client.Photon.Hashtable t = new ExitGames.Client.Photon.Hashtable();
        t.Add("CharacterClass", PhotonNetwork.LocalPlayer.CustomProperties["CharacterClass"]);
        PhotonNetwork.CurrentRoom.SetCustomProperties( t );

        _roomsCanvases.CurrentRoomCanvas.Show();

        Debug.Log("Custom room prop is: " + PhotonNetwork.CurrentRoom.CustomProperties["CharacterClass"]);
        Debug.Log("Custom Player prop is: " + PhotonNetwork.LocalPlayer.CustomProperties["CharacterClass"]);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Room creation fail: "+ message);
    }
}

using ExitGames.Client.Photon;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CustomDataType : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private MyCustomSerialization _customSerialization = new MyCustomSerialization();
    [SerializeField]
    private bool _sendAsTyped = true;
    // Start is called before the first frame update
    void Start()
    {                           //You can add other types here
        PhotonPeer.RegisterType(typeof(MyCustomSerialization), (byte)'M', MyCustomSerialization.Serialize, MyCustomSerialization.Deserialize);
    }

    // Update is called once per frame
    void Update()
    {
        if (_customSerialization.MyNumber != -1) {
            SendCustomSerialization(_customSerialization, _sendAsTyped);
            _customSerialization.MyNumber = -1;
            _customSerialization.MyString = string.Empty;
        }
    }

    private void SendCustomSerialization(MyCustomSerialization data, bool typed) {
        if (!typed)
            base.photonView.RPC("RPC_ReceiveMyCustomSerialization", RpcTarget.AllViaServer, MyCustomSerialization.Serialize(_customSerialization));
        else
            base.photonView.RPC("RPC_TypedReceivedMyCustomSerialization", RpcTarget.AllViaServer, _customSerialization);
    }

    [PunRPC]
    private void RPC_ReceiveMyCustomSerialization(byte[] datas) {
        MyCustomSerialization result = (MyCustomSerialization)MyCustomSerialization.Deserialize(datas);
        print("Received byte array: " + result.MyNumber + ", " + result.MyString);
    }

    [PunRPC]
    private void RPC_TypedReceivedMyCustomSerialization(MyCustomSerialization datas) {
        print("Received typed " + datas.MyNumber + ", " + datas.MyString);
    }
}

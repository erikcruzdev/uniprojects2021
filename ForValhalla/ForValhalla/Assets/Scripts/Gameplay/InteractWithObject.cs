using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InteractWithObject : MonoBehaviour
{
   
    // detects when the other transform is closer than closeDistance
    // this is faster than using Vector3.magnitude
    private PlayableCharacterBase _localPlayer;
    public float closeDistance = 5.0f;
    public GameObject Outline;
    public InteractionBar interactionBar;
    private InteractionOnTopOfObject _positionSetter;
    public GameObject ObjectToSpawn;

    private bool _isComplete = false;
    // Start is called before the first frame update
    private void Awake()
    {
        _positionSetter = GetComponent<InteractionOnTopOfObject>();
        _positionSetter.SetInteractableObject(InteractionController.Instance.FillInteraction);
    }
    void Start()
    {
        Outline.SetActive(false);

        foreach (PlayableCharacterBase character in SceneManager.Instance.PlayersOnRoom)
        {
            if (character.IsLocalPlayer)
            {
                _localPlayer = character;
                Debug.Log("GOT MY LOCAL PLAYER!!");
            }
        }
    }

    void OnEnable()
    {
        ((InteractionBar)interactionBar).OnMultipleInteractionComplete += SpawnObject;
    }
    void OnDisable()
    {
        ((InteractionBar)interactionBar).OnMultipleInteractionComplete -= SpawnObject;
    }

    public void SpawnObject()
    {
        Vector2 offset = Random.insideUnitCircle * 3f;
        Vector2 position = new Vector2(transform.position.x + offset.x, transform.position.y + offset.y);
        //if(PhotonNetwork.IsMasterClient)
         MasterManager.NetworkInstantiate(ObjectToSpawn, position, Quaternion.identity);
        //else
    }



    // Update is called once per frame
    void Update()
    {
        if (_localPlayer == null)
        {
            foreach (PlayableCharacterBase character in SceneManager.Instance.PlayersOnRoom)
            {
                if (character.IsLocalPlayer)
                {
                    _localPlayer = character;
                    Debug.Log("GOT MY LOCAL PLAYER!!");
                }
            }
        }

        if (_localPlayer!= null)
        {
            Vector3 offset = _localPlayer.transform.position - transform.position;
            float sqrLen = offset.sqrMagnitude;

            // square the distance we compare with
            if (sqrLen < closeDistance * closeDistance)
            {
                // Debug.Log("The other transform is close to me!");
                Outline.SetActive(true);
                _positionSetter.SetPosition(gameObject);
            }
            else
            {
                if (_positionSetter != null && _positionSetter.ui_Element.CurrentTarget == gameObject) _positionSetter.Hide();
                Outline.SetActive(false);
                //Debug.Log("The other transform is NOT close to me!");
            }
        }
        else {
            //Debug.Log("Local player is null");
        }

    }
}

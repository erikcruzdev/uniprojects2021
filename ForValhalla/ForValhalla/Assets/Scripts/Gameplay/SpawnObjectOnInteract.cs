using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SpawnObjectOnInteract : InteractableObject
{
    public GameObject ObjectToSpawn;

    protected override void Awake() {
        base.Awake();
        interactionBar = InteractionController.Instance.FillInteraction;
        _positionSetter.SetInteractableObject(InteractionController.Instance.FillInteraction);    
    }
    protected override void Start()
    {
        base.Start();
    }
    protected override void Update()
    {
        base.Update();
        if (IsClose && !HasRegisteredEvent) {
            ((InteractionBar)interactionBar).OnMultipleInteractionComplete += SpawnObject;
            HasRegisteredEvent = true;
        }
        else if (!IsClose && HasRegisteredEvent) {
            ((InteractionBar)interactionBar).OnMultipleInteractionComplete -= SpawnObject; 
            HasRegisteredEvent = false; 
        }
    }

    void OnEnable()
    {
        ((InteractionBar)interactionBar).OnMultipleInteractionComplete += SpawnObject;
        HasRegisteredEvent = true;
    }
    void OnDisable()
    {
        ((InteractionBar)interactionBar).OnMultipleInteractionComplete -= SpawnObject;
        HasRegisteredEvent = false;
    }
    public void SpawnObject()
    {
        Vector2 offset = Random.insideUnitCircle * 3f;
        Vector2 position = new Vector2(transform.position.x + offset.x, transform.position.y + offset.y);
        //if(PhotonNetwork.IsMasterClient)
        MasterManager.NetworkInstantiate(ObjectToSpawn, position, Quaternion.identity);
        //else
    }

}

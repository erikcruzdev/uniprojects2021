using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TileBase : InteractableObject
{
    public const string IS_DAMAGED = "isDamaged";
   // public GameObject ObjectToSpawn;
    private Animator _anim;

    [SerializeField] private bool _isDamaged;


    protected override void Awake() {
        base.Awake();
        interactionBar = InteractionController.Instance.TileInteraction;
        _positionSetter.SetInteractableObject(interactionBar);
        _anim = GetComponent<Animator>();
    
    }
    protected override void Start()
    {

        base.Start();
    }
    protected override void Update()
    {
        if (_isDamaged)
        {
            base.Update();
        }
        CheckDamageState();
        if (IsClose && !HasRegisteredEvent)
        {
            ((InteractionBar)interactionBar).OnMultipleInteractionComplete += FixTile;
            HasRegisteredEvent = true;
        }
        else if (!IsClose && HasRegisteredEvent)
        {
            ((InteractionBar)interactionBar).OnMultipleInteractionComplete -= FixTile;
            HasRegisteredEvent = false;
        }
        //Debug.Log("Interaction bar is " + interactionBar, interactionBar);
    }

    void CheckDamageState()
    {
        if (_anim.GetBool(IS_DAMAGED)!= _isDamaged) {

            _anim.SetBool(IS_DAMAGED, _isDamaged);
        } 
    }
    void OnEnable()
    {
        ((InteractionBar)interactionBar).OnMultipleInteractionComplete += FixTile;
        HasRegisteredEvent = true;
    }
    void OnDisable()
    {
        ((InteractionBar)interactionBar).OnMultipleInteractionComplete -= FixTile;
        HasRegisteredEvent = false;
    }
    public void FixTile()
    {
        LifeBar.Instance.UpdateLifeBar(0.2f, false);
        if (_isDamaged) _isDamaged = false;
    }

}

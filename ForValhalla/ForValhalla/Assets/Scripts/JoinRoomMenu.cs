using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoinRoomMenu : MonoBehaviour
{

    private RoomsCanvases _roomsCanvases;



    [SerializeField] private RoomListingsMenu _roomListingsMenu;

    public void FirstInitialize( RoomsCanvases canvas)
    {
        _roomsCanvases = canvas;
       // _createRoomMenu.FirstInitialize(canvas);
        _roomListingsMenu.FirstInitialize(canvas);
    }
}

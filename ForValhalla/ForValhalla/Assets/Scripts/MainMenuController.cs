using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    public static MainMenuController Instance;
    [SerializeField] private Button _joinButton;
    [SerializeField] private Button _createRoomButton;

    [SerializeField] private GameObject _joinCanvas;
    [SerializeField] private GameObject _createRoomCanvas;
    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        SetButtonsInteraction(false);
    }

    public void SetButtonsInteraction( bool interaction) {
        _joinButton.interactable = interaction;
        _createRoomButton.interactable = interaction;
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClick_Join() {
        _joinCanvas.SetActive(true);
    }

    public void OnClick_CreateRoom() {
        _createRoomCanvas.SetActive(true);
    }
}

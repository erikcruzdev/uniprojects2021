//using Newtonsoft.Json.Schema;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Manager/GameSettings")]
public class GameSettings : ScriptableObject
{
    [SerializeField] private string _gameVersion ="0.0.0";

    public string GameVersion => _gameVersion;
    [SerializeField] private string _nickName = "hue";

    public string Nickname {
        get {
            int value = Random.Range(0, 9999);
            return _nickName + value.ToString();

        }
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

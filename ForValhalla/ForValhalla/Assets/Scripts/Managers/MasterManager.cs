using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
    using UnityEditor;
#endif
using UnityEngine;

[CreateAssetMenu(menuName ="Singletons/MasterManager")]
public class MasterManager : SingletonScriptableObject<MasterManager>
{
    [SerializeField] private GameSettings gameSettings;
    public static GameSettings GameSettings { get { return Instance.gameSettings; } }

    [SerializeField]
    private List<NetworkPrefab> _networkedPrefabs = new List<NetworkPrefab>();

    public static GameObject NetworkInstantiate(GameObject obj, Vector3 position, Quaternion rotation) {

        foreach (NetworkPrefab networkedPrefab in Instance._networkedPrefabs)
        {
            if (networkedPrefab.Prefab == obj) {
                if (networkedPrefab.Path != string.Empty)
                {
                    GameObject result = PhotonNetwork.Instantiate(networkedPrefab.Path, position, rotation);
                    return result;
                }
                else {
                    Debug.LogError("Path is empty for game object name "+networkedPrefab.Prefab);
                    return null;
                }
            }
        }
        return null;
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void PopulateNetworkPrefabs() {
#if UNITY_EDITOR
        Instance._networkedPrefabs.Clear();

        GameObject[] result = Resources.LoadAll<GameObject>("");
        for (int i = 0; i < result.Length; i++)
        {
            if (result[i].GetComponent<PhotonView>() != null)
            {
               string path =  AssetDatabase.GetAssetPath(result[i]);
                Instance._networkedPrefabs.Add(new NetworkPrefab(result[i], path));
            }
        }
        //for debug only
        for (int i = 0; i < Instance._networkedPrefabs.Count; i++)
        {
            UnityEngine.Debug.Log(Instance._networkedPrefabs[i].Prefab.name + " " + Instance._networkedPrefabs[i].Path);
        }
#endif
    }
}

using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OwnershipTransfer : MonoBehaviourPun, IPunOwnershipCallbacks
{
    private void Awake()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }
    private void OnDestroy()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }
    public void OnOwnershipRequest(PhotonView targetView, Player requestingPlayer)
    {
        if (targetView != base.photonView) return;
        //Add checks here if you want to add some condition/rule to transfer 
        base.photonView.TransferOwnership(requestingPlayer);
    }

    public void OnOwnershipTransfered(PhotonView targetView, Player previousOwner)
    {
        if (targetView != base.photonView) return;
    }

    public void OnOwnershipTransferFailed(PhotonView targetView, Player senderOfFailedRequest)
    {
        Debug.Log("Transfer Ownership Failed!!!!");
       // throw new System.NotImplementedException();
    }

    // private void OnMouseDown()
    // {
    //     base.photonView.RequestOwnership();
    // }
    //public void RemoveOwnership() {
    //    base.photonView. = null;
    //}

    public void RequestOwnership() {
        base.photonView.RequestOwnership();
    }
}

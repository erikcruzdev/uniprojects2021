using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerListing : MonoBehaviourPunCallbacks
{
    [SerializeField] private TextMeshProUGUI _text;

    [SerializeField] private Image _image;

    [SerializeField] public Color _myColor;

    [SerializeField] public Color _otherPlayerColor;

    public PlayerListingMenu _listOfPlayers;
    public Player Player { get; private set; }
    public bool Ready = false;
      
    public void SetPlayerInfo(Player player) {
        Player = player;
        SetPlayerText(player);
        if (player.IsLocal)
        {
            _image.color = _myColor;
        }
        else { 
             _image.color = _otherPlayerColor;
        }
    }
    // Start is called before the first frame update
    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        if (targetPlayer != null && targetPlayer == Player) {
            if (changedProps.ContainsKey("CharacterClass")) {
                SetPlayerText(targetPlayer);
                if (targetPlayer.IsMasterClient && !PhotonNetwork.LocalPlayer.IsMasterClient) {
                    if (targetPlayer.CustomProperties.ContainsKey("CharacterClass"))
                    {
                        var targetPlayerClass = (CharacterClass)targetPlayer.CustomProperties["CharacterClass"];
                        var result = targetPlayerClass == CharacterClass.WARRIOR ? CharacterClass.DWARF : CharacterClass.WARRIOR;
                        Debug.Log("TROCOU A CLASSE DO MEU PERSONAGEM meu player � master? "+PhotonNetwork.IsMasterClient);
                        ChangeMyClass(result);
                    }
                 }
            }
        }
    }

    private void ChangeMyClass(CharacterClass charClass)
    {
         ExitGames.Client.Photon.Hashtable _myCustomProperty = new ExitGames.Client.Photon.Hashtable();
        _myCustomProperty["CharacterClass"] = charClass;
        // to remove
        //_myCustomProperty.Remove("RandomNumber");
        // PhotonNetwork.LocalPlayer.CustomProperties = _myCustomProperty;
        PhotonNetwork.SetPlayerCustomProperties(_myCustomProperty);
    }
    private void SetPlayerText(Player player) {
        //int result = -1;
        var classText = string.Empty;
        if (player.CustomProperties.ContainsKey("CharacterClass"))
        {
            var result = (CharacterClass)player.CustomProperties["CharacterClass"];
            var text = result == CharacterClass.WARRIOR ? "Warrior" : "Dwarf";
            classText = text;
        }
        _text.text = classText.ToString() + " - " + player.NickName;
    }
}

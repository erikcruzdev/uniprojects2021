using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerListingMenu : MonoBehaviourPunCallbacks
{
    [SerializeField] private bool DEBUG_MasterCanStartMatchAlone = false;
    [SerializeField] private PlayerListing _playerListings;
    [SerializeField] private Transform _content;

    [SerializeField] private RoomsCanvases _roomsCanvases;

    [SerializeField] private TextMeshProUGUI _readyUpText;

    [SerializeField] private GameObject _changeClassButton;

    private List<PlayerListing> _listing = new List<PlayerListing>();

    public List<PlayerListing> PlayerListings => _listing;

    private bool _isReady = false;
    private void Awake()
    {
        GetCurrentRoomPlayers();
    }

    public override void OnEnable()
    {
        base.OnEnable();
        SetReadyUp(false);
        GetCurrentRoomPlayers();
        if (!PhotonNetwork.IsMasterClient)
        {
            _changeClassButton.SetActive(false);
        }
    }

    private void SetReadyUp(bool state)
    {
        _isReady = state;
        if (_isReady)
        {
            _readyUpText.text = "Ready";
        }
        else
        {
            _readyUpText.text = "Not ready";
        }
        // SetReadyUp(false);

    }

    public override void OnDisable()
    {
        base.OnDisable();
        for (int i = 0; i < _listing.Count; i++)
        {
            Destroy(_listing[i].gameObject);
        }
        _listing.Clear();
    }
    public void FirstInitialize(RoomsCanvases canvas)
    {
        _roomsCanvases = canvas;
    }
    private void GetCurrentRoomPlayers()
    {

        if (!PhotonNetwork.IsConnected) return;
        if (PhotonNetwork.CurrentRoom == null || PhotonNetwork.CurrentRoom.Players == null) return;

        foreach (KeyValuePair<int, Player> playerInfo in PhotonNetwork.CurrentRoom.Players)
        {
            AddPlayerListing(playerInfo.Value);
        }
    }

    public void AddPlayerListing(Player player)
    {
        int index = _listing.FindIndex(x => x.Player == player);
        if (index != -1)
        {
            _listing[index].SetPlayerInfo(player);
        }
        else
        {
            PlayerListing listing = Instantiate(_playerListings, _content);
            if (listing != null)
            {
                listing.SetPlayerInfo(player);
                _listing.Add(listing);
            }
        }
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        AddPlayerListing(newPlayer);
    }

    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        base.OnMasterClientSwitched(newMasterClient);
        _roomsCanvases.CurrentRoomCanvas.LeaveRoomMenu.OnClick_LeaveRoom();
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        int index = _listing.FindIndex(x => x.Player == otherPlayer);
        Debug.Log("Index is:: " + index);
        if (index != -1)
        {
            Destroy(_listing[index].gameObject);
            _listing.RemoveAt(index);
        }
    }

    public void OnClick_StartGame()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            //ONLY MASTER CAN START GAME WITH THE CODE BELLOW
            if (!DEBUG_MasterCanStartMatchAlone)
            {
                for (int i = 0; i < _listing.Count; i++)
                {
                    if (_listing[i].Player != PhotonNetwork.LocalPlayer)
                    {
                        if (!_listing[i].Ready) return;
                    }
                }
            }

            PhotonNetwork.CurrentRoom.IsOpen = false;
            PhotonNetwork.CurrentRoom.IsVisible = false;
            PhotonNetwork.LoadLevel(1);
        }
    }
    public void OnClick_ReadyUp()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            SetReadyUp(!_isReady);
            base.photonView.RPC("RPC_ChangeReadyState", RpcTarget.MasterClient, PhotonNetwork.LocalPlayer, _isReady);
        }
    }

    [PunRPC]
    private void RPC_ChangeReadyState(Player player, bool ready) {

        int index = _listing.FindIndex(x => x.Player == player);
        Debug.Log("Index is:: " + index);
        if (index != -1)
        {
            _listing[index].Ready = ready;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickInstantiate : MonoBehaviour
{
    [SerializeField] private List<GameObject> _prefab = new List<GameObject>();

    private void Awake()
    {
        Vector2 offset = Random.insideUnitCircle * 3f;

        Vector2 position = new Vector2(transform.position.x + offset.x, transform.position.y + offset.y);

        foreach (GameObject go in _prefab)
        {
            MasterManager.NetworkInstantiate(go, position, Quaternion.identity);
        }

    }
}

using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RaiseEventExample : MonoBehaviourPun
{
    private const byte COLOR_EVENT = 0;
    private SpriteRenderer _spriteRenderer;
    // Start is called before the first frame update
    void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += NewtworkingClient_EventReceived;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= NewtworkingClient_EventReceived;
    }

    private void NewtworkingClient_EventReceived(EventData obj)
    {
        if (obj.Code == COLOR_EVENT) {
            object[] data = (object[])obj.CustomData;
            float r = (float)data[1];
            float g = (float)data[2]; 
            float b = (float)data[3];

            _spriteRenderer.color = new Color(r, g, b, 1f);

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (base.photonView.IsMine && Input.GetKeyDown(KeyCode.Space)) ChangeColor();
    }

    private void ChangeColor() {
        float r = UnityEngine.Random.Range(0f, 1f);
        float g = UnityEngine.Random.Range(0f, 1f);
        float b = UnityEngine.Random.Range(0f, 1f);

        _spriteRenderer.color = new Color(r, g, b, 1f);

        object[] data = new object[] { base.photonView.ViewID, r, g, b }; //create a new object

        PhotonNetwork.RaiseEvent(COLOR_EVENT, data, RaiseEventOptions.Default, SendOptions.SendUnreliable);
        //Can be unreliable, different than rpc. An event 
    }


}

using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RoomListing : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text;

    public RoomInfo RoomInfo { get; private set; }

    public void SetRoomInfo(RoomInfo roomInfo) {
        RoomInfo = roomInfo;
        var charClass = "";
       
        if (roomInfo.CustomProperties.ContainsKey("CharacterClass")) {
            var custom_num = (CharacterClass)roomInfo.CustomProperties["CharacterClass"];
            charClass = string.Concat(custom_num.ToString(), ", ");
        }
        _text.text = charClass + roomInfo.Name;
    }
    // Start is called before the first frame update

    public void OnClick_Button() {
        PhotonNetwork.JoinRoom(RoomInfo.Name);
        PhotonNetwork.ConnectUsingSettings();
    }


}

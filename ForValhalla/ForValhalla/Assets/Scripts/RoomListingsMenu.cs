using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomListingsMenu : MonoBehaviourPunCallbacks
{
    [SerializeField] private RoomListing _roomListings;
    [SerializeField] private Transform _content;

    private List<RoomListing> _listing = new List<RoomListing>();
    private RoomsCanvases _roomsCanvases;

    public void FirstInitialize(RoomsCanvases canvas) {
        _roomsCanvases = canvas;
    }

    public override void OnJoinedRoom()
    {
        _roomsCanvases.CurrentRoomCanvas.Show();
        _content.DestroyChildren();
        _listing.Clear();
        if (PhotonNetwork.IsMasterClient) return;
        if (PhotonNetwork.MasterClient.CustomProperties.ContainsKey("CharacterClass") && PhotonNetwork.MasterClient != PhotonNetwork.LocalPlayer)
        {
            var targetPlayerClass = (CharacterClass)PhotonNetwork.MasterClient.CustomProperties["CharacterClass"];
            var result = targetPlayerClass == CharacterClass.WARRIOR ? CharacterClass.DWARF : CharacterClass.WARRIOR;
            Debug.Log("TROCOU A CLASSE DO MEU PERSONAGEM! meu player � master? " + PhotonNetwork.IsMasterClient);
            ChangeMyClass(result);
        }        
    }

    private void ChangeMyClass(CharacterClass charClass)
    {
        ExitGames.Client.Photon.Hashtable _myCustomProperty = new ExitGames.Client.Photon.Hashtable();
        _myCustomProperty["CharacterClass"] = charClass;
        // to remove
        //_myCustomProperty.Remove("RandomNumber");
        // PhotonNetwork.LocalPlayer.CustomProperties = _myCustomProperty;
        PhotonNetwork.SetPlayerCustomProperties(_myCustomProperty);
    }
    public override void OnRoomListUpdate(List<RoomInfo> roomList) {

        foreach (RoomInfo info in roomList)
        {
            //remove room from list
            if (info.RemovedFromList)
            {
                int index = _listing.FindIndex(x => x.RoomInfo.Name == info.Name);
                Debug.Log("Index is:: " + index);
                if (index != -1) {
                    Destroy(_listing[index].gameObject);
                    _listing.RemoveAt(index);
                }
            }
            //Add room to list
            else
            {
                int index = _listing.FindIndex(x => x.RoomInfo.Name == info.Name);
                if (index == -1)
                {
                    RoomListing listing = Instantiate(_roomListings, _content);
                    if (listing != null)
                    {

                        listing.SetRoomInfo(info);
                        _listing.Add(listing);
                    }
                }
                else {
                    _listing[index].SetRoomInfo(info);
                }
            }
        }

    }
}

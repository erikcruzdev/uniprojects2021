using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomsCanvases : MonoBehaviour
{
    [SerializeField] private CreateRoomCanvas _createRoomCanvas;
    public CreateRoomCanvas CreateOrJoinRoomCanvas => _createRoomCanvas;

    [SerializeField] private JoinRoomMenu _joinRoomCanvas;
    public JoinRoomMenu JoinRoomCanvas => _joinRoomCanvas;

    [SerializeField] private CurrentRoomCanvas _currentRoomCanvas;
    public CurrentRoomCanvas CurrentRoomCanvas => _currentRoomCanvas;

    private void OnEnable()
    {
        FirstInitialize();
    }

    public void OnClick_BackToMainMenu() {
        _currentRoomCanvas.Hide();
        _createRoomCanvas.gameObject.SetActive(false);
        _joinRoomCanvas.gameObject.SetActive(false);
    }

    private void FirstInitialize() {
        _currentRoomCanvas.FirstInitialize(this);
        _createRoomCanvas.FirstInitialize(this);
        _joinRoomCanvas.FirstInitialize(this);

    }
}

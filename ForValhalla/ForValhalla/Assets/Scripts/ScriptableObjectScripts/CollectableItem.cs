using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "CollectableItem", menuName = "ScriptableObjects/CollectableItem", order = 3)]
public class CollectableItem : ScriptableObject
{
    public GameObject PrefabToSpawn;
}

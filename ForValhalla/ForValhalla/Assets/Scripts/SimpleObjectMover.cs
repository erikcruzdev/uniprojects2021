using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleObjectMover : MonoBehaviourPun, IPunObservable
{
    private Animator _animator;


    [SerializeField] private float _moveSpeed = 0.05f;

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
       // if (stream.IsWriting)
       // {
       //     stream.SendNext(transform.position);
       // }
       // else if (stream.IsReading) {
       //     transform.position = (Vector2)stream.ReceiveNext();
       // }
    }

    private void Awake()
    {
        _animator = GetComponent<Animator>();
       
    }

  

    // Update is called once per frame
    void Update()
    {
        if (base.photonView.IsMine) {
           
            float x = Input.GetAxisRaw("Horizontal");
            float y = Input.GetAxisRaw("Vertical");

            transform.position += ((new Vector3(x, y, 0f)) * _moveSpeed) * Time.deltaTime;

            UpdateMovingBoolean((x != 0f || y != 0f));
        }
    }

    private void UpdateMovingBoolean(bool moving) {
        _animator.SetBool("IsMoving", moving);
    }
}

using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestConnect : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Connecting to server...", this);
        //Add instead of 1, the name of the player ID;
        //AuthenticationValues authValues = new AuthenticationValues("1");

        //PhotonNetwork.AuthValues = authValues;
        PhotonNetwork.SendRate = 20; //20 is the default value
        PhotonNetwork.SerializationRate = 10; // higher values is not good. 10 means 10 times per second
        PhotonNetwork.SerializationRate = 5;
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.NickName = MasterManager.GameSettings.Nickname;
        PhotonNetwork.GameVersion = MasterManager.GameSettings.GameVersion;
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster() {
        print("Connected to server");
        print(PhotonNetwork.LocalPlayer.NickName);
        if(!PhotonNetwork.InLobby)
            PhotonNetwork.JoinLobby();
        MainMenuController.Instance.SetButtonsInteraction(true);
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        print("Disconnected for reason: " + cause.ToString());
    }

    public override void OnJoinedLobby()
    {
        print("Joined lobby");
        //To find friend
       // PhotonNetwork.FindFriends(new string[] { "1" }); 
    }

    //to update friend list
   
    /*
    public override void OnFriendListUpdate(List<FriendInfo> friendList)
    {
        base.OnFriendListUpdate(friendList);
        foreach (FriendInfo info in friendList)
        {
            Debug.Log("Friend info received " + info.UserId + " is online? " + info.IsOnline);
        }
    }*/

    // Update is called once per frame
    void Update()
    {
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;
using Photon.Realtime;

public class TimeManager : MonoBehaviourPunCallbacks
{
    [SerializeField] private float _maxTimer = 100;
    [SerializeField] private float timeRemaining;
    public bool timeIsOver = false;
    float minutes;
    float seconds;
    float milisseconds; //This is totally optional but ya know the tutorial had it so why not add it too?
    public TextMeshProUGUI timeText;

    [SerializeField]
    private float _timeToMasterSycn;

    private float _timerToMasterSync;

    // Start is called before the first frame update
    void Start(){
        timeRemaining = _maxTimer;
    }

    void TimerDisplay(float _time){

        _time += 1; //This is totally optinal and only used for convencional purposes

        minutes = Mathf.FloorToInt(_time/60);
        seconds = Mathf.FloorToInt(_time%60);
        if (timeIsOver) {
            minutes = 0;
            seconds = 0;
        }
        milisseconds = (_time % 1) * 1000;

        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    [PunRPC]
    public void RPC_SyncMatchTimer(float timeToSync){
        timeRemaining = timeToSync;
        TimerDisplay(timeRemaining);
        Debug.Log("RECEBI RPC PARA SYNCAR MEU TEMPO!!!");
    }

    [PunRPC]
    public void RPC_TimeIsOver() {
        timeIsOver = true;
        timeRemaining = 0;
        TimerDisplay(timeRemaining);
        GameOverScreen.Instance.SetGameOverTextAndEnable("Time is Over!", 1f);
        Debug.Log("ACABOOOOOOOOO RPC!!!");
    }

    // Update is called once per frame
    void Update(){              

        if(!timeIsOver){
            if (timeRemaining >= 0){
                timeRemaining -= Time.deltaTime;
                TimerDisplay(timeRemaining);
            }
            else{
                Debug.Log("Tempo acabou D: ");
                timeRemaining = 0;
                if (PhotonNetwork.IsMasterClient) {
                    base.photonView.RPC("RPC_TimeIsOver", RpcTarget.All);
                    Debug.Log("ACABOOOOOOOOO MASTER!!!");
                    //TimerDisplay(timeRemaining);
                }
                timeIsOver = true;

            }
        }

        if (PhotonNetwork.IsMasterClient && !timeIsOver) {
            _timerToMasterSync += Time.deltaTime;
            if (_timerToMasterSync >= _timeToMasterSycn)
            {
                Debug.Log("MANDEI RPC PARA SYNCAR O TEMPO DO CLIENT");
                base.photonView.RPC("RPC_SyncMatchTimer", RpcTarget.Others, timeRemaining);
                _timerToMasterSync = 0;
            }
        }
    }
}
